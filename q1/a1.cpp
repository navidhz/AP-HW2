#include<iostream>

int main(){
	int a{10};
	int* const b{&a}; //const pointer
	b = &a;
	(*b)++;
	a++;
	std::cout << a << " " << b << " " << *b;
	return 0;
}

//output
//12 0x7fffc8d32c2c 12
