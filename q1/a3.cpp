#include<iostream>

int main(){
	char a{'a'};
	const char* name{"Amir Jahanshahi"};
	const char* p1{name}; //
	std::cout << *p1 << *(p1+1) << *(p1+2) << std::endl;
	p1 = &a;//
	std::cout << *p1 << *(p1+1) << *(p1+2) << std::endl;
	p1 = name;
	//*p1 = 'b';//
	//char* p2{name};//	
	return 0;
}

/*
./a3.cpp: In function ‘int main()’:
./a3.cpp:11:8: error: assignment of read-only location ‘* p1’
  *p1 = 'b';//
        ^~~
./a3.cpp:12:15: error: invalid conversion from ‘const char*’ to ‘char*’ [-fpermissive]
  char* p2{name};//
               ^
./a3.cpp:12:8: warning: unused variable ‘p2’ [-Wunused-variable]
  char* p2{name};//

*/

/*
Ami
a4�
*/
