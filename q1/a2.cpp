#include<iostream>

int main()
{
	const int a{10};
	int c{20};
	int d{30};
	const int* b{&a};
	b = &c;
	std::cout << a << " " << b << " " << *b;
	int* const e{&c};
	e = &d; //const pointer
	std::cout << d << " " << e << " " << *e;
	return 0;
}
/*
error: assignment of read-only variable ‘e’
  e = &d;
       ^
*/
