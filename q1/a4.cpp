#include<iostream>

int main(){
	int* p1{new int[10]}; // make a dynamic array
	int* p2[10];// array of pointer
	
	//functions
	int (*p3)(int[]); // function is a pointer to a function that takes two arguments of type int and int that returns an int
	int (*p4[10])(int [][10]); // function is a pointer to an array of function that takes two arguments of type int and int that returns an int
	// 10*10*10 matrix
	int (*p5)[10]{new int [10][10]}; //
	return 0;
}
